#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Bools {
  char s;
  char t;
  char e;
  char b;
  char n;
  char v;
};

struct Flags {
  char s;
  char t;
  char e;
  char b;
  char n;
  char v;
};

void PrintFile(FILE *file);
int OptionCounter(char *argv[], int argc);
int OptionChecker(char *argv[], int argc, struct Bools *bools,
                  struct Flags *flags);
void OptionRealize(char *argv[], int argc, struct Bools *bools);
void RemoveAll();
void PrintHelp();
FILE *FuncForSFlag(char *filename, FILE *fileTmp, int flag);
FILE *FuncForTFlag(char *filename, FILE *fileTmp);
FILE *FuncForEFlag(char *filename, FILE *fileTmp);
FILE *FuncForBFlag(char *filename, FILE *fileTmp);
FILE *FuncForNFlag(char *filename, FILE *fileTmp);
FILE *FuncForVFlag(char *filename, FILE *fileTmp);
