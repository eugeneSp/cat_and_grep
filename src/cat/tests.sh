#!/bin/bash

for flag in "-b" "-e" "-n" "-s" "-t" "-v"; do

    cat $flag test_folder/1.txt > test_folder/cat_output.txt

    ./s21_cat $flag test_folder/1.txt > test_folder/s21_cat_output.txt

    if diff test_folder/cat_output.txt test_folder/s21_cat_output.txt >/dev/null; then
        echo "$flag test_folder/1.txt: Test passed"
    else
        echo "$flag test_folder/1.txt: Test failed"
    fi
done

./s21_cat --squeeze-blank test_folder/1.txt > test_folder/s21_cat_output.txt
if diff test_folder/squeeze-blank_test.txt test_folder/s21_cat_output.txt >/dev/null; then
    echo "--squeeze-blank test_folder/1.txt: Test passed"
else
    echo "$flag test_folder/1.txt: Test failed"
fi

./s21_cat --number test_folder/1.txt > test_folder/s21_cat_output.txt
if diff test_folder/number_test.txt test_folder/s21_cat_output.txt >/dev/null; then
    echo "--number test_folder/1.txt: Test passed"
else
    echo "--number test_folder/1.txt: Test failed"
fi

./s21_cat --number-nonblank test_folder/1.txt > test_folder/s21_cat_output.txt
if diff test_folder/number-nonblank_test.txt test_folder/s21_cat_output.txt >/dev/null; then
    echo "--number-nonblank test_folder/1.txt: Test passed"
else
    echo "--number-nonblank test_folder/1.txt: Test failed"
fi

rm -f test_folder/cat_output.txt
rm -f test_folder/s21_cat_output.txt
