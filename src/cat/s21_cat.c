#include "s21_cat.h"

int main(int argc, char *argv[]) {
  struct Bools bools = {0, 0, 0, 0, 0, 0};
  struct Flags flags = {'s', 't', 'e', 'b', 'n', 'v'};

  if (OptionChecker(argv, argc, &bools, &flags) == 1) {
    OptionRealize(argv, argc, &bools);
    RemoveAll();
  }

  return 0;
}

int OptionChecker(char *argv[], int argc, struct Bools *bools,
                  struct Flags *flags) {
  int i = 1;
  int flag = 1;

  while (i < argc) {
    if (argv[i][0] == '-') {
      if (argv[i][1] == flags->s || strcmp(argv[i], "--squeeze-blank") == 0) {
        bools->s = 1;
      } else if (argv[i][1] == flags->t) {
        bools->t = 1;
        bools->v = 1;
      } else if (argv[i][1] == flags->e) {
        bools->e = 1;
        bools->v = 1;
      } else if (argv[i][1] == flags->b ||
                 strcmp(argv[i], "--number-nonblank") == 0) {
        bools->b = 1;
      } else if (argv[i][1] == flags->n || strcmp(argv[i], "--number") == 0) {
        bools->n = 1;
      } else if (argv[i][1] == flags->v) {
        bools->v = 1;
      } else if (strcmp(argv[i], "--help") == 0) {
        PrintHelp();
        flag = -1;
      } else if (strcmp(argv[i], "-E") == 0) {
        bools->e = 1;
      } else if (strcmp(argv[i], "-T") == 0) {
        bools->t = 1;
      } else {
        fprintf(stderr, "s21_cat: Not found that flag\n");
      }
    }
    i++;
  }

  return flag;
}

void OptionRealize(char *argv[], int argc, struct Bools *bools) {
  FILE *file = NULL;

  for (int i = 1; i < argc; i++) {
    if (argv[i][0] != '-') {
      if ((file = fopen(argv[i], "r")) != NULL) {
        if (bools->s == 1) {
          file = FuncForSFlag(argv[i], file, 1);
        }
        if (bools->t == 1) {
          file = FuncForTFlag(argv[i], file);
        }
        if (bools->b == 1) {
          file = FuncForBFlag(argv[i], file);
        }
        if (bools->e == 1) {
          file = FuncForEFlag(argv[i], file);
        }
        if (bools->n == 1 && bools->b == 0) {
          file = FuncForNFlag(argv[i], file);
        }
        if (bools->v == 1) {
          file = FuncForVFlag(argv[i], file);
        }
        if (file != NULL) {
          PrintFile(file);
          fclose(file);
        }
      } else {
        fprintf(stderr, "s21_cat: Error: could not open file %s\n", argv[i]);
      }
    }
  }
}

FILE *FuncForSFlag(char *filename, FILE *fileTmp, int flag) {
  FILE *file = NULL;
  FILE *tmp = NULL;
  char tmpFileName[32] = ".tmp.1.txt";
  char data[255];
  int counter = 0;
  int blankLine = 0;

  if (fileTmp == NULL) {
    file = fopen(filename, "r");
    tmp = fopen(tmpFileName, "w");
  } else {
    file = fileTmp;
    tmp = fopen(tmpFileName, "w");
  }

  while (fgets(data, 255, file) != NULL) {
    if (flag == 1) {
      if (strcmp(data, "\n") == 0) {
        if (blankLine == 0) {
          blankLine = 1;
          fputs(data, tmp);
        }
      } else {
        blankLine = 0;
        fputs(data, tmp);
      }
    } else {
      if (strcmp(data, "\n") == 0) {
        counter++;
      }
      if (counter <= 1) {
        fputs(data, tmp);
      }
    }
  }

  fclose(file);
  fclose(tmp);

  return fopen(tmpFileName, "r");
}

FILE *FuncForTFlag(char *filename, FILE *fileTmp) {
  FILE *file = NULL;
  FILE *tmp = NULL;
  char tmpFileName[32] = ".tmp.2.txt";

  if (fileTmp == NULL) {
    file = fopen(filename, "r");
    tmp = fopen(tmpFileName, "w");
  } else {
    file = fileTmp;
    tmp = fopen(tmpFileName, "w");
  }

  char c = ' ';
  while ((c = fgetc(file)) != EOF) {
    if (c == '\t') {
      fputs("^I", tmp);
    } else {
      fputc(c, tmp);
    }
  }
  fclose(file);
  fclose(tmp);

  return fopen(tmpFileName, "r");
}

FILE *FuncForEFlag(char *filename, FILE *fileTmp) {
  FILE *file = NULL;
  FILE *tmp = NULL;
  char tmpFileName[32] = ".tmp.3.txt";

  if (fileTmp == NULL) {
    file = fopen(filename, "r");
    tmp = fopen(tmpFileName, "w");
  } else {
    file = fileTmp;
    tmp = fopen(tmpFileName, "w");
  }

  char c = ' ';
  while ((c = fgetc(file)) != EOF) {
    if (c != '$') {
      if (c == '\n') {
        fprintf(tmp, "$\n");
      } else {
        fprintf(tmp, "%c", c);
      }
    }
  }
  fclose(file);
  fclose(tmp);

  return fopen(tmpFileName, "r");
}

FILE *FuncForBFlag(char *filename, FILE *fileTmp) {
  char tmpFileName[32] = ".tmp.4.txt";
  FILE *file = NULL;
  FILE *tmp = NULL;
  char strInFile[255];
  int i = 1;

  if (fileTmp == NULL) {
    file = fopen(filename, "r");
    tmp = fopen(tmpFileName, "w");
  } else {
    file = fileTmp;
    tmp = fopen(tmpFileName, "w");
  }

  while (fgets(strInFile, 255, file) != NULL) {
    if (strcmp(strInFile, "\n") != 0) {
      int numSpaces = 5;
      if (i >= 10 && i < 100) {
        numSpaces = 4;
      } else if (i >= 100 && i < 1000) {
        numSpaces = 3;
      }

      fprintf(tmp, "%*s%d\t%s", numSpaces, "", i, strInFile);
      i++;
    } else {
      fprintf(tmp, "%s", strInFile);
    }
  }

  fclose(file);
  fclose(tmp);

  return fopen(tmpFileName, "r");
}

FILE *FuncForNFlag(char *filename, FILE *fileTmp) {
  char tmpFileName[32] = ".tmp.5.txt";
  FILE *file = NULL;
  FILE *tmp = NULL;
  char strInFile[255];
  int i = 1;

  if (fileTmp == NULL) {
    file = fopen(filename, "r");
    tmp = fopen(tmpFileName, "w");
  } else {
    file = fileTmp;
    tmp = fopen(tmpFileName, "w");
  }

  while (fgets(strInFile, 255, file) != NULL) {
    int numSpaces;
    if (i < 10) {
      numSpaces = 5;
    } else if (i < 100) {
      numSpaces = 4;
    } else {
      numSpaces = 3;
    }
    fprintf(tmp, "%*s%d\t%s", numSpaces, "", i, strInFile);
    i++;
  }

  fclose(file);
  fclose(tmp);

  return fopen(tmpFileName, "r");
}

FILE *FuncForVFlag(char *filename, FILE *fileTmp) {
  char tmpFileName[32] = ".tmp.6.txt";
  FILE *file = NULL;
  FILE *tmp = NULL;

  if (fileTmp == NULL) {
    file = fopen(filename, "r");
    tmp = fopen(tmpFileName, "w");
  } else {
    file = fileTmp;
    tmp = fopen(tmpFileName, "w");
  }

  int c = ' ';
  while ((c = fgetc(file)) != EOF) {
    if ((((c >= 0 && c <= 31) || c == 127)) && (c != 9 && c != 10)) {
      fputs("^X", tmp);
    } else if (c > 127 && c < 160) {
      fprintf(tmp, "M-^%c", c - 64);
    } else {
      fputc(c, tmp);
    }
  }

  fclose(file);
  fclose(tmp);

  return fopen(tmpFileName, "r");
}

void RemoveAll() {
  remove(".tmp.1.txt");
  remove(".tmp.2.txt");
  remove(".tmp.3.txt");
  remove(".tmp.4.txt");
  remove(".tmp.5.txt");
  remove(".tmp.6.txt");
}

void PrintFile(FILE *file) {
  char buffer[255];
  while (fgets(buffer, 255, file) != NULL) {
    printf("%s", buffer);
  }
}

int OptionCounter(char *argv[], int argc) {
  int i = 0, counter = 0;
  while (i < argc) {
    if (argv[i][0] == '-') {
      counter++;
    }
    i++;
  }
  return counter;
}

void PrintHelp() {
  printf("Usage: s21_cat [OPTION]... [FILE]...\n");
  printf("Concatenate FILE(s) to standard output.\n\n");
  printf("  -b, --number-nonblank   number non-empty output lines\n");
  printf("  -e                      display $ at end of each line\n");
  printf("  -n, --number            number all output lines\n");
  printf("  -s, --squeeze-blank     suppress repeated empty output lines\n");
  printf("  -t, --show-tabs         display TAB characters as ^I\n");
  printf("      --help              display this help and exit\n");
}
